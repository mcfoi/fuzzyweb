package eu.marcofoi.wicket;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormSubmitBehavior;
import org.apache.wicket.markup.html.form.Form;

public class BlurSubmitBehavior extends AjaxFormSubmitBehavior {

	private static final long serialVersionUID = 1L;

	public BlurSubmitBehavior(Form<?> form) {
		super(form, "onblur");
	}

	@Override  
    protected void onSubmit(AjaxRequestTarget target) {  
      //we do nothing but we could do something interesting  
      System.out.println("BlurSubmitBehavior. onBlur triggered");  
    }
	
	@Override  
    protected void onError(AjaxRequestTarget target) {  
    } 
}
