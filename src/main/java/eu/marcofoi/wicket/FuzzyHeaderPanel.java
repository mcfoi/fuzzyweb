package eu.marcofoi.wicket;

import org.apache.wicket.markup.html.panel.Panel;

import eu.marcofoi.wicket.comps.languageform.LanguageForm;

public class FuzzyHeaderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1985844150754438571L;

	public FuzzyHeaderPanel(String id) {
		super(id);		
		
		
		LanguageForm languageForm = new LanguageForm("languageForm","_plain");
        add(languageForm);  
		
	}
}
