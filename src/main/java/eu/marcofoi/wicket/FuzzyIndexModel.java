package eu.marcofoi.wicket;

import java.io.Serializable;

public class FuzzyIndexModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 82260350332810724L;
	private double value;
	private int function;
	private double functionLBound;
	private double functionUBound;
	private double weight;
	
	public FuzzyIndexModel() {
		super();
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getFunction() {
		return function;
	}

	public void setFunction(int function) {
		this.function = function;
	}

	public double getFunctionLBound() {
		return functionLBound;
	}

	public void setFunctionLBound(double functionLBound) {
		this.functionLBound = functionLBound;
	}

	public double getFunctionUBound() {
		return functionUBound;
	}

	public void setFunctionUBound(double functionUBound) {
		this.functionUBound = functionUBound;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

}
