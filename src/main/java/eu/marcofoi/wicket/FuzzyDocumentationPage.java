package eu.marcofoi.wicket;

public class FuzzyDocumentationPage extends FuzzyTemplateWebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6719492325110957098L;

	public FuzzyDocumentationPage() {
		super();
		add(contentPanel = new FuzzyDocumentationPanel(CONTENT_ID).setMarkupId(CONTENT_ID));
	}

}
