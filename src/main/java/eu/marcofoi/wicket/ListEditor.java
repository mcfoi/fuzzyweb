package eu.marcofoi.wicket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.wicket.markup.html.form.IFormModelUpdateListener;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;

public abstract class ListEditor<T> extends RepeatingView implements
		IFormModelUpdateListener, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<T> items;

	public ListEditor(String id, IModel<?> model) {
		super(id, model);
		items = new ArrayList<T>();
	}

	protected abstract void onPopulateItem(ListItem<T> item);

	public void addItem(T value) {
		items.add(value);
		ListItemCustom<T> item = new ListItemCustom<T>(newChildId(), items.size() - 1);
		add(item);
		onPopulateItem(item);
	}

	protected void onBeforeRender() {
		if (!hasBeenRendered()) {
			Object o = getDefaultModelObject();
			IModel im = getDefaultModel();
			//was getModelObject()
			items = new ArrayList<T>((Collection<? extends T>) o);
			for (int i = 0; i < items.size(); i++) {
				ListItem<T> li = new ListItem<T>(newChildId(), i);
				add(li);
				onPopulateItem(li);
			}
		}
		super.onBeforeRender();
	}

	public void updateModel() {
		setDefaultModelObject(items);
	}
}
