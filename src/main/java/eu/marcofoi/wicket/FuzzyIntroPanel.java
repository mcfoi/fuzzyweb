package eu.marcofoi.wicket;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

import eu.marcofoi.wicket.comps.editableformpanel.EditableFormPanel;
import eu.marcofoi.wicket.comps.languageform.LanguageForm;

public class FuzzyIntroPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 602825339364056106L;

	public FuzzyIntroPanel(String id) {
		super(id);

		add(new Link("loadStartPanel") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 206153372435247851L;

			@Override
			public void onClick() {
				getPage().replace(new EditableFormPanel(FuzzyTemplateWebPage.CONTENT_ID).setMarkupId(FuzzyTemplateWebPage.CONTENT_ID));			
			}
		});
		
		LanguageForm languageForm = new LanguageForm("languageForm","_plain");
        add(languageForm);  
	}
	
}
