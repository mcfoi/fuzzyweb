package eu.marcofoi.wicket;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class FuzzyDocumentationPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2998645233388524396L;

	public FuzzyDocumentationPanel(String id) {
		super(id);
		// 
	}

	public FuzzyDocumentationPanel(String id, IModel<?> model) {
		super(id, model);
		// 
	}

}
