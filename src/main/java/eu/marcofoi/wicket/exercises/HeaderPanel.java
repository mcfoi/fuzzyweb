package eu.marcofoi.wicket.exercises;

import java.util.Calendar;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

public class HeaderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2881195039495577305L;

	public HeaderPanel(String id) {
		super(id);
		
		WebMarkupContainer informationBox = new WebMarkupContainer ("informationBox");
		Label labelYearNumber = new Label("yearNumber", Integer.toString(Calendar.getInstance().get(Calendar.YEAR)));
		informationBox.add(labelYearNumber);
		add(informationBox);
		labelYearNumber.setVisible(true); //Setting to false, due to wicket:enclosure in markup, will hide whole id:informationBox
		//If there are no new messages, hide informationBox
		//informationBox.setVisible(false);
	}

}
