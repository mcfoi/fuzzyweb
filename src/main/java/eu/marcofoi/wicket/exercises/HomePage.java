package eu.marcofoi.wicket.exercises;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import eu.marcofoi.wicket.SimpleLoginPage;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {
		super(parameters);

		add(new Label("version", getApplication().getFrameworkSettings().getVersion()));

		add(new Link<Object>("linkjugtemplate") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 4632522205214320461L;

			@Override
			public void onClick() {
				//we redirect browser to another page.
				
                //setResponsePage(JugTemplate.class);
				
                JugTemplate anotherPage = new JugTemplate();
            	setResponsePage(anotherPage);
			}
		});
		

		add(new Link<Object>("linklogin") {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2363305608115904652L;

			@Override
			public void onClick() {
				//we redirect browser to another page.
				
                //setResponsePage(JugTemplate.class);
				
				SimpleLoginPage loginPage = new SimpleLoginPage();
            	setResponsePage(loginPage);
			}
		});
    }
}
