package eu.marcofoi.wicket.exercises;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import eu.marcofoi.wicket.ListEditor;

public class JugTemplate extends WebPage {

	private static final long serialVersionUID = 1L;

	public static final String CONTENT_ID = "contentComponent";

	private Component headerPanel;
	private Component menuPanel;
	private Component footerPanel;
	private Component indexesPanel;

	private Label firstLabel;
	private Label secondLabel;
	private Label disabledLabel;

	private Form resizableForm;
	private ListEditor listEditor;

	public JugTemplate() {
		add(headerPanel = new HeaderPanel("headerPanel")
				.setMarkupId("headerPanel"));
		add(menuPanel = new MenuPanel("menuPanel").setMarkupId("menuPanel"));
		add(footerPanel = new FooterPanel("footerPanel")
				.setMarkupId("footerPanel"));

		add(indexesPanel = new IndexesPanel("indexesPanel"));

		Label labelPutContentHere = new Label(CONTENT_ID,
				"This text was styled using 'AttributeModifier' and 'AttributeAppender'!");
		labelPutContentHere.add(new AttributeModifier("style", "color:red;"));
		labelPutContentHere.add(new AttributeAppender("style",
				"font-weight:bold;"));
		add(labelPutContentHere);
		Label labelAnotherLabel = new Label("anotherLabel",
				"This label uses setRenderBodyOnly(true)");
		labelAnotherLabel.setRenderBodyOnly(true);
		add(labelAnotherLabel);

		firstLabel = new Label("selfTogglingLabel",
				"First label: rendered as a DIV from hardcoded markup");
		secondLabel = new Label("selfTogglingLabel",
				"Second label: rendered as a SPAN by onComponentTag method") {
			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onComponentTag(ComponentTag tag) {
				super.onComponentTag(tag);
				// Turn the h1 tag to a span
				tag.setName("span");
				// Add formatting style
				tag.put("style", "font-weight:bold");
			}
		};

		add(firstLabel);
		add(new Link<Object>("reload") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
			}
		});

		disabledLabel = new Label("disabledLabel",
				"This lable can be disabled!") {
			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTagBody(MarkupStream markupStream,
					ComponentTag tag) {

				if (!isEnabled())
					replaceComponentTagBody(markupStream, tag,
							"(The component is disabled)");
				else
					super.onComponentTagBody(markupStream, tag);
			}
		};
		add(disabledLabel);
		add(new Link<Object>("disable") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				// ((Label)get("disabledLabel")).setEnabled(false);
				disabledLabel.setEnabled(!disabledLabel.isEnabled());
			}
		});

		Fragment fragment = new Fragment("contentArea", "fragmentId", this);
		fragment.add(new AttributeModifier("style", "background:red;"));
		add(fragment);
		// setOutputMarkupId(true);

		resizableForm = new Form("resizableForm");

		List<String> list = new ArrayList<String>();
		list.add("Field1");

		IModel<?> listEditorModel = Model.ofList(list);

		listEditor = new ListEditor("listEditor", listEditorModel) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 2475969581770966501L;

			@Override
			protected void onPopulateItem(ListItem item) {
				
			}
		};
		
		TextField tf = new TextField("username", Model.of(""));
		listEditor.addItem(tf);
		

		resizableForm.add(new Button("add") {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6136693571814543800L;

			public void onSubmit() {
				listEditor.addItem(new TextField<String>("username", Model
						.of("")));
			}
		}.setDefaultFormProcessing(false));

		resizableForm.add(listEditor);
		add(resizableForm);

		add(new Link<Object>("repeat") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				// ((Label)get("disabledLabel")).setEnabled(false);
				// disabledLabel.setEnabled(!disabledLabel.isEnabled());
				// getPage().get("repeatHost").getMarkup();
				// getPage().get("repeatHost").
				// indexesPanel.add(new )
			}
		});
		// add(new Label("repeatHost", "---"));
		RepeatingView listIndexes = new RepeatingView("repeatHost");
		// listIndexes.add(childs)

		IModel<String> timeStampModel = new Model<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {
				return new Date().toString();
			}
		};
		add(new Label("timeStamp", timeStampModel));
	}

	@Override
	protected void onInitialize() {
		super.onInitialize(); // NOTE: call to parent GOES FIRST!!
	}

	@Override
	protected void onBeforeRender() {
		if (contains(firstLabel, true))
			replace(secondLabel);
		else
			replace(firstLabel);

		super.onBeforeRender(); // NOTE: call to parent goes LAST in method
	}

	public Component getMenuPanel() {

		return (Panel) get("menuPanel");
	}

	public Component getHeaderPanel() {

		return (Panel) get("headerPanel");
	}

	public Component getFooterPanel() {

		return (Panel) get("footerPanel");
	}

}
