package eu.marcofoi.wicket.exercises;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class IndexesPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3860017406062913596L;

	public IndexesPanel(String id) {
		super(id);
		// 
	}

	public IndexesPanel(String id, IModel<?> model) {
		super(id, model);
		// 
	}

}
