package eu.marcofoi.wicket.comps.languageform;

import java.util.Locale;

import org.apache.wicket.markup.html.form.ImageButton;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

public class LanguageButton extends ImageButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2617705367442772493L;
	ILanguageButtonBehaviour behav;
	
	public LanguageButton(String id, ResourceReference resourceReference, ILanguageButtonBehaviour behav) {
		super(id, resourceReference);
		this.behav = behav;
	}

	public LanguageButton autoSetVisibility(char sep){
		
		boolean isVisible = this.behav.computeVisibility(sep);
		return (LanguageButton) this.setVisible(isVisible);

	}

	protected void changeUserLocaleTo(Locale locale) {
		getSession().setLocale(locale);
	}
	
}
