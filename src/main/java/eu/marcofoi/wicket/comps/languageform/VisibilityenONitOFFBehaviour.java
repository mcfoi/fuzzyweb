package eu.marcofoi.wicket.comps.languageform;

import java.io.Serializable;

public class VisibilityenONitOFFBehaviour implements ILanguageButtonBehaviour, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VisibilityenONitOFFBehaviour() {
	}

	@Override
	public boolean computeVisibility(char currentSepar) {
		if ('.' == currentSepar) {
			return true;
		} else {
			return false;
		}
	}


}
