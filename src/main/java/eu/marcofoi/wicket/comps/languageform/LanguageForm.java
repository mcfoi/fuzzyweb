package eu.marcofoi.wicket.comps.languageform;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.PackageResourceReference;

public class LanguageForm extends Form {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 699828594384295948L;
	LanguageButton it_on;
	LanguageButton en_on;
	LanguageButton it_off;
	LanguageButton en_off;

	//char currentSep;
	
	public LanguageForm(String id, String imageSuffix) {
		super(id);
		
		PackageResourceReference resourceReference_it_on = new PackageResourceReference(getClass(), "it_on"+imageSuffix+".png");
		PackageResourceReference resourceReference_it_off = new PackageResourceReference(getClass(), "it_off"+imageSuffix+".png");
		PackageResourceReference resourceReference_en_on = new PackageResourceReference(getClass(), "en_on"+imageSuffix+".png");
		PackageResourceReference resourceReference_en_off = new PackageResourceReference(getClass(), "en_off"+imageSuffix+".png");
		
				//dd(new Image("packageResPicture", resourceReference));
				
		ILanguageButtonBehaviour enONitOFFBehaviour = new VisibilityenONitOFFBehaviour();
		
		ILanguageButtonBehaviour enOFFitOnBehaviour = new VisibilityenOFFitOnBehaviour();

		it_on = (LanguageButton) new LanguageButton("it_on", resourceReference_it_on, enOFFitOnBehaviour).setMarkupId("it_on").setEnabled(false);

		en_on = (LanguageButton) new LanguageButton("en_on", resourceReference_en_on, enONitOFFBehaviour).setMarkupId("en_on").setEnabled(false);
		
		it_off = (LanguageButton) new LanguageButton("it_off", resourceReference_it_off, enONitOFFBehaviour) {

			private static final long serialVersionUID = 4104859650858135695L;

			@Override
			public void onSubmit() {
				changeUserLocaleTo(new Locale("it", "IT"));
			}
		}.setMarkupId("it_off");

		en_off = (LanguageButton) new LanguageButton("en_off", resourceReference_en_off, enOFFitOnBehaviour) {

			private static final long serialVersionUID = -3277014296858326031L;

			@Override
			public void onSubmit() {
				changeUserLocaleTo(new Locale("en", "GB"));
			}
		}.setMarkupId("en_off");

	}

	/*
	@Override
	protected void onConfigure() {
		super.onConfigure();
		currentSep = getCurrentSeparator();
	}
	*/

	private char getCurrentSeparator() {

		Locale loc = getSession().getLocale();

		DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(loc);
		DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
		return symbols.getDecimalSeparator();

	}

	@Override
	protected void onBeforeRender() {
		
		char sep = getCurrentSeparator();

		add(it_on.autoSetVisibility(sep));
		add(en_on.autoSetVisibility(sep));
		add(it_off.autoSetVisibility(sep));
		add(en_off.autoSetVisibility(sep));

		super.onBeforeRender();
	}

}
