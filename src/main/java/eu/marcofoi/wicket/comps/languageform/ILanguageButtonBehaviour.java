package eu.marcofoi.wicket.comps.languageform;

public interface ILanguageButtonBehaviour {
	
	public boolean computeVisibility(char sep);

}
