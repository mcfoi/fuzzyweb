package eu.marcofoi.wicket.comps.editableformpanel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import eu.marcofoi.fuzzylogic.ClusterRunner;
import eu.marcofoi.wicket.FuzzyIndexModel;
import eu.marcofoi.wicket.FuzzyIndexModelArrayParser;
import eu.marcofoi.wicket.comps.languageform.LanguageForm;

public class EditableFormPanel extends Panel {

	private static final long serialVersionUID = 1L;
	private List<FuzzyIndexModel> indexes;
    
    @SuppressWarnings("unchecked")
    public EditableFormPanel( String id ) {
        super(id);
           
        LanguageForm languageForm = new LanguageForm("languageForm","");
        add(languageForm);      
		
        indexes = new ArrayList<FuzzyIndexModel>();
        Form multiform = new Form("multiform", Model.of("")){
            /**
			 * 
			 */
			private static final long serialVersionUID = 431360549096783205L;

			@Override
            public void onSubmit() {
                System.out.println("onSubmit of multiform");
                ArrayList<FuzzyIndexModel> fuzzyIndexModelArray = (ArrayList<FuzzyIndexModel>) this.get("items").getDefaultModel()
                        .getObject();
                ClusterRunner cr = FuzzyIndexModelArrayParser.getClusterRunner(fuzzyIndexModelArray);
                cr.run();
                this.setModelObject(cr.getResult());
            }
        };
        multiform.setOutputMarkupId(true);
      
        TextField fuzzyResultField = new TextField("fuzzyIndex", Model.of("")){
            
            /**
			 * 
			 */
			private static final long serialVersionUID = -226979304552736455L;

			@Override
            protected void onConfigure() {
                super.onConfigure();
                if(getParent().get("multiform") != null){
                    if (getParent().get("multiform").getDefaultModelObject()!=null){
                        this.setModelObject(getParent().get("multiform").getDefaultModelObject());
                    }
                }    
            }
            
        };
        fuzzyResultField.setType(Double.class);
        add(fuzzyResultField);
        
        // multiform.setDefaultModel(new CompoundPropertyModel<T>(this));

        final LinkedHashMap<Integer, String> functions = new LinkedHashMap<Integer, String>();
        functions.put(0, "Ascending (Liao, 2002)");
        functions.put(1, "Descending (Liao, 2002)");

        final IModel dropDownModel = new Model(){
            /**
			 * 
			 */
			private static final long serialVersionUID = 7526043743963192373L;

			@Override
            public Serializable getObject() {
                return new ArrayList<Integer>(functions.keySet());
            }
        };
        
        ListView itemsListView;
        // the list of items
        multiform.add(itemsListView = new ListView("items", new PropertyModel(this, "indexes")){

            private static final long serialVersionUID = 1L;

            @Override
            protected void populateItem( ListItem item ) {

            	Form multiform = (Form) getParent().get("multiform");
            	
                item.add(new TextField("value", new PropertyModel(item.getModelObject(), "value")).setOutputMarkupId(true)/*.add(new BlurSubmitBehavior(multiform))*/);

                // item.add(new DropDownChoice<String>("function", new PropertyModel(item.getModelObject(), "function"), functions));
                item.add(new DropDownChoice<Integer>("function", new PropertyModel(item.getModelObject(), "function"), dropDownModel,
                        new ChoiceRenderer(){
                            /**
							 * 
							 */
							private static final long serialVersionUID = -4899299762124723600L;
							@Override
                            public String getDisplayValue( Object object ) {
                                return functions.get(object);
                            }
                            @Override
                            public String getIdValue( Object object, int index ) {
                                return object.toString();
                            }
                }).setOutputMarkupId(true));

                item.add(new TextField("functionLBound", new PropertyModel(item.getModelObject(), "functionLBound")).setOutputMarkupId(true));

                item.add(new TextField("functionUBound", new PropertyModel(item.getModelObject(), "functionUBound")).setOutputMarkupId(true));

                item.add(new TextField("weight", new PropertyModel(item.getModelObject(), "weight")).setOutputMarkupId(true)/*.add(new BlurSubmitBehavior(multiform))*/);
            }
        });
        
        //#################
        itemsListView.setReuseItems(true); //Required for allowing ListView in Form to reuse its components 
        //and not to create new ones every time

        AjaxSubmitLink addItemButton = new AjaxSubmitLink("addItemButton"){

            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit( AjaxRequestTarget target, Form< ? > form ) {
            	System.out.println("onSubmit of addItemButton");
                indexes.add(new FuzzyIndexModel());
                target.add(form);
            }

            @Override
            protected void onError( AjaxRequestTarget target, Form< ? > form ) {
                target.add(form);
            }
        };
        addItemButton.setDefaultFormProcessing(false);// Prevents the form submission
        addItemButton.setMarkupId("addItemButton");// Prevents JS from messing up
        //addItemButton.setOutputMarkupId(true);
        multiform.add(addItemButton);

        
        AjaxSubmitLink removeItemButton = new AjaxSubmitLink("removeItemButton"){

            private static final long serialVersionUID = 1L;

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setEnabled(indexes.size() > 0);
            }

            @Override
            protected void onSubmit( AjaxRequestTarget target, Form< ? > form ) {
            	System.out.println("onSubmit of removeItemButton");
                indexes.remove(indexes.size() - 1);
                target.add(form);
            }

            @Override
            protected void onError( AjaxRequestTarget target, Form< ? > form ) {
                target.add(form);
            }
        };
        removeItemButton.setDefaultFormProcessing(false);// Prevents the form submission
        removeItemButton.setMarkupId("removeItemButton");// Prevents JS from messing up 
        //removeItemButton.setOutputMarkupId(true);        
        multiform.add(removeItemButton);

        SubmitLink doSomethingUseful = new SubmitLink("doSomethingUseful"){

            private static final long serialVersionUID = 1L;

            @Override
            protected void onConfigure() {
                super.onConfigure();
                setEnabled(indexes.size() > 0);
            }

            @Override
            public void onSubmit() {
            	System.out.println("onSubmit of SubmitLink");
                //System.out.println(indexes);
            }
        };
        doSomethingUseful.setOutputMarkupId(true);
        doSomethingUseful.setMarkupId("doSomethingUseful");// Prevents JS from messing up        
        multiform.add(doSomethingUseful);
        
        
        add(multiform);
    }
}
