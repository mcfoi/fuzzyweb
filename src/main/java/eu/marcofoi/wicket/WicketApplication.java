package eu.marcofoi.wicket;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.session.ISessionStore;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see eu.marcofoi.wicket.Start#main(String[])
 */
public class WicketApplication extends WebApplication {
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends Page> getHomePage() {
		// return HomePage.class;
		return FuzzyStartWebPage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		
		getRequestCycleSettings().setResponseRequestEncoding("UTF-8"); 
        getMarkupSettings().setDefaultMarkupEncoding("UTF-8"); 
        
		super.init();

		// add your configuration here
		//remove thread monitoring from resource watcher: this is REQUIRED by Google App Engine that prevents apps to span multiple threads.
		this.getResourceSettings().setResourcePollFrequency(null);
	}

	/*
    @Override
    protected ISessionStore newSessionStore() {
      return new HttpSessionStore(this);
    }
    */
	
}
