package eu.marcofoi.wicket;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebPage;

public class FuzzyTemplateWebPage extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1992830495988300817L;

	public static final String CONTENT_ID = "contentPanel";

	protected Component contentPanel;
	public FuzzyTemplateWebPage() {
		
		System.out.println(this.getSession().getLocale());
		
		add(new FuzzyHeaderPanel("headerPanel").setMarkupId("headerPanel"));
		
		add(new FuzzyFooterPanel("footerPanel").setMarkupId("footerPanel"));
	}

}
