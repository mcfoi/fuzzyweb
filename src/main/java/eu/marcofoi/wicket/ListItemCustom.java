package eu.marcofoi.wicket;

import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.AbstractReadOnlyModel;

public class ListItemCustom<T> extends Item<T> {

	 /**
	 * 
	 */
	private static final long serialVersionUID = -1665420208500439185L;

	public ListItemCustom(String id, int index)
	   {
	      super(id, index);
	      setModel(new ListItemModel());
	   }
	 
	   private class ListItemModel extends AbstractReadOnlyModel<T>
	   {
	      /**
		 * 
		 */
		private static final long serialVersionUID = 1520956023180609214L;

		public T getObject()
	      {
	         return ((ListEditor<T>)ListItemCustom.this.getParent()).items.get(getIndex());
	      }
	   }
	
}
