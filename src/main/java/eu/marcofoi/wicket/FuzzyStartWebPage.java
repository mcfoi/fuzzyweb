package eu.marcofoi.wicket;

public class FuzzyStartWebPage extends FuzzyTemplateWebPage {	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FuzzyStartWebPage() {
		super();
		add(contentPanel = new FuzzyIntroPanel(CONTENT_ID).setMarkupId(CONTENT_ID));
	}

}
