package eu.marcofoi.wicket;

import java.util.ArrayList;

import eu.marcofoi.fuzzylogic.ClusterRunner;
import eu.marcofoi.fuzzylogic.ISFunction;
import eu.marcofoi.fuzzylogic.SFunctionASCRamp;
import eu.marcofoi.fuzzylogic.SFunctionDESCRamp;

public class FuzzyIndexModelArrayParser {

	private FuzzyIndexModelArrayParser() {
	}

	public static ClusterRunner getClusterRunner(
			ArrayList<FuzzyIndexModel> array) {

		ISFunction[] sfs = new ISFunction[array.size()];
		double[] values = new double[array.size()];
		double[] weights = new double[array.size()];
		
		int i = -1;
		for (FuzzyIndexModel fm : array) {
			i++;
			values[i] = fm.getValue();
			
			ISFunction isfunction = getFunction(fm.getFunction());
			isfunction.setLower_bound(fm.getFunctionLBound());
			isfunction.setUpper_bound(fm.getFunctionUBound());
			sfs[i] = isfunction;			

			weights[i] = fm.getWeight();

		}

		String software = "";
		String cluster = "";

		boolean verbose = true;
		
		ClusterRunner cr = new ClusterRunner(software, cluster, sfs, values, weights, verbose);
		
		return cr;

	}

	private static ISFunction getFunction(int i) {

		int id = Integer.valueOf(i);
		ISFunction function = null;
		
		switch (id) {
		case 0:
			function = new SFunctionASCRamp();
			break;
		case 1:
			function = new SFunctionDESCRamp();
			break;
		}
		
		return function;
	}
}
