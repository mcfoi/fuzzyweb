package eu.marcofoi.wicket;

import eu.marcofoi.wicket.exercises.JugTemplate;
import eu.marcofoi.wicket.exercises.LoginPanel;


public class SimpleLoginPage extends JugTemplate {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2277814233625059936L;

	public SimpleLoginPage() {
		super();		
		replace(new LoginPanel(CONTENT_ID));
		getMenuPanel().setVisible(false);
		
	}

}
