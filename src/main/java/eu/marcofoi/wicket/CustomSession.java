package eu.marcofoi.wicket;

import java.util.Locale;

import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

public class CustomSession extends WebSession {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2135116108540713080L;

	public CustomSession(Request request) {
		super(request);
		this.setLocale(new Locale("en", "GB"));
	}
	
	@Override
	public Locale getLocale() {
		//System.out.println(super.getLocale().toString());
		return super.getLocale();
	}

}
